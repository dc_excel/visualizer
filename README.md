![](img/heatscatter.png)
# Visualizer For Excel
Control chart object fill color, border color, marker size, line color, line style, and data labels via conditional formatting or regular formatting of the source cells.

This allows you to quickly format a chart without needing to edit every individual data point or add helper columns to your dataset.

# Installation
This repository contains the code in both xlsm format, and as an Excel add-in. You can install the add-in from the developer tab as described here

https://support.office.com/en-us/article/add-or-remove-add-ins-in-excel-0af570c4-5cf3-4fa9-9b88-403625a0b460

## Excel Version
This add-in was developed and tested on Excel 2016. However, this add-in does not work on the chart types added in 2016 (Sunburst, Waterfall, Boxplot, etc) which are currently unavailable in the VBA object model.

# Use
When you click on a chart in Excel, you'll receive the additional Chart Tools tabs for Design and Format. On the design tab, you will find a new group for Excel Visualizer.


## Adding a chart to Visualizer
![](img/toolbar.png)

Once you have a chart actively selected in your workbook, click Add Chart from the ribbon under Chart Tools -> Design -> Excel Visualizer.

*If the cells of your chart's source do not have any formatting, expect the components of the chart to "disappear", as they will change to a white fill color on a white background.

## Editing chart format

![](img/linechartsample.png)

Edit the below formatting options of your data source to update the corresponding chart element. 

In most cases changes will occur in real-time, however not all the actions trigger the Excel chart to refresh. If the formatting does not immediately take effect, you can generally underline one of the values and then remove it to trigger a refresh.

| Cell Format Element        | Chart Element           |
| ------------- |---------------|
| Cell background color  | 	Fill color |
| Underlined     | Has data label      |
| Font color | Data label font color      |
| Cell border - top | Element border (Respects existance, color, style, and weight)      |
| Cell border - right | Connecting line (Respects existance, color, style, and weight)      |
| Bold | Has marker      |
| Font size | Marker size      |


## Charts without individual data points
For charts like the 2D Area chart which are made of one solid object rather than individual chart elements, the formatting of the series header effects the entire series.

## Removing a chart from Visualizer
You can stop the chart format from updating based on the cells by clicking the Remove Chart button from the ribbon. The chart will maintain all the formatting created using the add-in, but will no longer update based on cell formatting.

## Remote labels
One of the most annoying things about Excel charting, especially with scatter plots, is the data label can only be the value of the cells used to make the chart, that is, unless you want to manually change all the labels by clicking on them.

By clicking the Advanced Settings ribbon button, you can specify a range of cells to use as the labels in the chart. These will be the default options when you underline a cell indicating you'd like a label to be on that data point.

![](img/threegroupscatter.png)

# Portability
For any charts that have not been Removed from Visualizer, settings are saved in the custom document properties of the workbook, and they will be restored upon opening if you have the add-in imstalled.

Once the chart is formatted, the add-in is not necessary to view the chart or open the workbook, allowing the document to be shared with users who do not have the add-in.