Attribute VB_Name = "RibbonSetup"
Dim MyRibbon As IRibbonUI

Sub MyAddInInitialize(Ribbon As IRibbonUI)
    Set MyRibbon = Ribbon
End Sub

Sub InvalidateRibbon()
    'Rebuilds the ribbon
    On Error Resume Next
    MyRibbon.Invalidate
    
    If Err.Number > 0 Then
        Err.Clear
    End If
    On Error GoTo 0
End Sub


Sub AddActiveChart()
    'Adds the currently selected chart into the array
    If Not g_ChartFormatter Is Nothing Then
        g_ChartFormatter.AddChart
    End If
    InvalidateRibbon

End Sub

Sub RemoveActiveChart()

    If Not g_ChartFormatter Is Nothing Then
        g_ChartFormatter.RemoveChart
    End If
    InvalidateRibbon

End Sub

Sub LaunchSettings()
    ChartFormatSettings.Show
End Sub



Sub NestedIfsVisualizerGetVisible(control As IRibbonControl, ByRef MakeVisible)

        MakeVisible = True

End Sub

Sub NestedIfsVisualizerGetEnabled(control As IRibbonControl, ByRef MakeEnabled)
    
    Dim b As Boolean
    On Error Resume Next
    b = g_ChartFormatter.ChartExists
    
    Select Case control.ID
      Case "NestedIfsVisualizerButton01", "NestedIfsVisualizerButton11"
        MakeEnabled = Not b
      Case "NestedIfsVisualizerButton02", "NestedIfsVisualizerButton03", "NestedIfsVisualizerButton12", "NestedIfsVisualizerButton13"
        MakeEnabled = b
      Case Else
        MakeEnabled = True
    End Select
    Err.Clear
    On Error GoTo 0

End Sub

Sub NestedIfsVisualizerGetLabel(ByVal control As IRibbonControl, ByRef Labeling)

    Select Case control.ID

      Case "NestedIfsVisualizerGroupA", "NestedIfsVisualizerGroupB"
        Labeling = "Excel Visualizer"
      Case "NestedIfsVisualizerButton01", "NestedIfsVisualizerButton11"
        Labeling = "Add Chart"
      Case "NestedIfsVisualizerButton02", "NestedIfsVisualizerButton12"
        Labeling = "Remove Chart"
      Case "NestedIfsVisualizerButton03", "NestedIfsVisualizerButton13"
        Labeling = "Advanced Settings"
    End Select
   
End Sub

Sub NestedIfsVisualizerGetImage(control As IRibbonControl, ByRef RibbonImage)

    Select Case control.ID
      
      Case "NestedIfsVisualizerButton01", "NestedIfsVisualizerButton11"
        RibbonImage = "ChartFormatAxis"
      Case "NestedIfsVisualizerButton02", "NestedIfsVisualizerButton12"
        RibbonImage = "CancelRequest"
      Case "NestedIfsVisualizerButton03", "NestedIfsVisualizerButton13"
        RibbonImage = "AdministrationHome"
    End Select

End Sub

Sub NestedIfsVisualizerGetSize(control As IRibbonControl, ByRef Size)

Const Large As Integer = 1
Const Small As Integer = 0

    Size = Large

End Sub

Sub NestedIfsVisualizerRunMacro(control As IRibbonControl)

 Select Case control.ID
  
  Case "NestedIfsVisualizerButton01", "NestedIfsVisualizerButton11"
    Application.Run "AddActiveChart"
  Case "NestedIfsVisualizerButton02", "NestedIfsVisualizerButton12"
    Application.Run "RemoveActiveChart"
  Case "NestedIfsVisualizerButton03", "NestedIfsVisualizerButton13"
    Application.Run "LaunchSettings"
 End Select
    
End Sub

Sub NestedIfsVisualizerGetScreentip(control As IRibbonControl, ByRef Screentip)

    Select Case control.ID
      
      Case "NestedIfsVisualizerButton01", "NestedIfsVisualizerButton11"
        Screentip = "Activate"
      Case "NestedIfsVisualizerButton02", "NestedIfsVisualizerButton12"
        Screentip = "Deactivate"
      Case "NestedIfsVisualizerButton03", "NestedIfsVisualizerButton13"
        Screentip = "Advanced Settings"
    End Select

End Sub


Sub NestedIfsVisualizerGetSupertip(control As IRibbonControl, ByRef Supertip)

    Select Case control.ID
      
      Case "NestedIfsVisualizerButton01", "NestedIfsVisualizerButton11"
        Supertip = "Connect active chart to the formatting engine"
      Case "NestedIfsVisualizerButton02", "NestedIfsVisualizerButton12"
        Supertip = "Remove active chart from the formatting engine"
      Case "NestedIfsVisualizerButton03", "NestedIfsVisualizerButton13"
        Supertip = "Customize advanced settings such as the range to use for labeling data points"
    End Select

End Sub

