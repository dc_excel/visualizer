VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ConditionalChart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private WithEvents c_Chart As Chart
Attribute c_Chart.VB_VarHelpID = -1
Private c_LabelRange As Range

Public Property Set Chart(pChart As Chart)

    Set c_Chart = pChart

End Property

Public Property Get Chart() As Chart

    Set Chart = c_Chart

End Property

Public Property Get LabelRange() As Range
    Set LabelRange = c_LabelRange
End Property
Public Property Set LabelRange(pRange As Range)
    Set c_LabelRange = pRange
End Property

Private Function GetChartType(pChartType As Double) As String

    Select Case pChartType
    
        Case -4101, 4, 65, 63, 64, 66, 67
            GetChartType = "Line"
        Case -4102, 70, 71, 69, 5, 68
            GetChartType = "Pie"
        Case -4151, 82, 81
            GetChartType = "Radar"
        Case -4169, 74, 75, 72, 73
            GetChartType = "Scatter"
        Case 88, 89, 90, 91
            GetChartType = "Stock"
        Case 83, 84, 85, 86
            GetChartType = "Surface"
        Case 87, 15
            GetChartType = "Bubble"
        Case -4098, 78, 79, 1, 76, 77
            GetChartType = "Area"
        Case 60, 61, 62, 57, 58, 59, 102, 103, 104, 95, 96, 97, 109, 110, 111
            GetChartType = "Bar"
        Case 54, -4100, 105, 98, 112, 55, 56, 51, 52, 53, 99, 100, 101, 92, 93, 94, 106, 107, 108
            GetChartType = "Column"
        Case -4120, 80
            GetChartType = "Doughnut"
        Case Else
            GetChartType = "Bar"
        End Select

End Function


Public Sub FormatChart()
    
    Dim l_ChartSeries As Series
    Dim l_SeriesRange As Range
    Dim l_SeriesTitle As Range
    Dim l_SeriesType As Double
    Dim l_ChartType As String
    Dim l_Point As Point
    Dim l_Count As Double
    Dim l_Cell As Range
    Dim l_SeriesTitleFormula As String
    
    With c_Chart
        
        For Each l_ChartSeries In .FullSeriesCollection
        
            With l_ChartSeries
            
                l_SeriesType = .ChartType
                
                'Exit for new charts that do not work with vba
                Select Case l_SeriesType
                    Case 118, 117, 123, 119, 120, 121, 122 'Histogram, treemap, funnel, waterfall, sunburst,box plot, pareto
                        Exit For
                End Select
                
                'gather info about chart range
                l_ChartType = GetChartType(l_SeriesType)
                Set l_SeriesRange = Range(SUBFIELD(.Formula, ",", 3))
                
                l_SeriesTitleFormula = Replace(SUBFIELD(.Formula, ",", 1), "=SERIES(", "")
                If Len(l_SeriesTitleFormula) = 0 Then
                    Set l_SeriesTitle = Nothing
                Else
                    Set l_SeriesTitle = Range(Replace(SUBFIELD(.Formula, ",", 1), "=SERIES(", ""))
                End If
                
                If Not l_SeriesTitle Is Nothing Then
                
                    'Alter the color if the header has a color
                    If l_SeriesTitle.DisplayFormat.Interior.ColorIndex <> -4142 Then
                        On Error Resume Next
                        .Format.Fill.ForeColor.RGB = ColorToRGB(l_SeriesTitle.DisplayFormat.Interior.Color)
                        Err.Clear
                        On Error GoTo 0
                    End If
                    
                    If l_SeriesTitle.DisplayFormat.Borders(xlEdgeRight).LineStyle <> -4142 Then
                        On Error Resume Next
                        .Format.Line.Visible = msoFalse
                        .Format.Line.Visible = msoTrue
                        .Format.Line.DashStyle = ConvertLineToDash(l_SeriesTitle.DisplayFormat.Borders(xlEdgeRight).LineStyle)
                        .Format.Line.Weight = l_SeriesTitle.DisplayFormat.Borders(xlEdgeRight).Weight
                        .Format.Line.ForeColor.RGB = l_SeriesTitle.DisplayFormat.Borders(xlEdgeRight).Color
                        Err.Clear
                        On Error GoTo 0
                    Else
                        On Error Resume Next
                        .Format.Line.Visible = msoFalse
                        Err.Clear
                        On Error GoTo 0
                    End If
                    
                    If l_SeriesTitle.DisplayFormat.Borders(xlEdgeTop).LineStyle <> -4142 Then
                    
                    End If
                End If
                
                
            
                l_Count = UBound(.Values)
                
                For j = 1 To l_Count
                    Set l_Point = .Points(j)
                    Set l_Cell = l_SeriesRange.Cells(j)
                    
                    On Error Resume Next
                    With l_Point
                        If l_Cell.DisplayFormat.Font.Bold Then
                            .MarkerStyle = xlMarkerStyleAutomatic
                            .MarkerSize = l_Cell.DisplayFormat.Font.Size
                            .MarkerBackgroundColor = l_Cell.DisplayFormat.Interior.Color
                        Else
                            .MarkerStyle = xlMarkerStyleNone
                        End If
                        
                        
                        
                        
                        .Format.Fill.ForeColor.RGB = l_Cell.DisplayFormat.Interior.Color
                        
                        

                        
                        'XlUnderlineStyle enumeration
                        If l_Cell.DisplayFormat.Font.Underline = xlUnderlineStyleSingle Then
                            .HasDataLabel = True
                            .DataLabel.Format.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = ColorToRGB(l_SeriesRange.Cells(j).DisplayFormat.Font.Color)
                            If c_LabelRange Is Nothing Then
                                .DataLabel.Caption = l_SeriesRange.Cells(j).Text
                            Else
                                .DataLabel.Caption = c_LabelRange.Cells(j).Text
                            End If
                            
                        Else
                            .HasDataLabel = False
                        End If
                        
                        
                        'Format line chart line based on right  edge border
                        If l_Cell.DisplayFormat.Borders(xlEdgeRight).LineStyle <> -4142 Then
                            Select Case l_ChartType
                                Case "Scatter", "Line"
                                    Err.Clear
                                    .Format.Line.Visible = msoFalse
                                    .Format.Line.Visible = msoTrue
                                    .Format.Line.DashStyle = ConvertLineToDash(l_Cell.DisplayFormat.Borders(xlEdgeRight).LineStyle)
                                    .Format.Line.Weight = l_Cell.DisplayFormat.Borders(xlEdgeRight).Weight
                                    .Format.Line.ForeColor.RGB = l_Cell.DisplayFormat.Borders(xlEdgeRight).Color
                            End Select
                        Else
                            Select Case l_ChartType
                                Case "Scatter", "Line"
                                    .Format.Line.Visible = msoFalse
                            End Select
                        
                        End If
                        
                        
                       'Format border based on top edge border
                       If l_Cell.DisplayFormat.Borders(xlEdgeTop).LineStyle <> -4142 Then
                        
                            Select Case l_ChartType
                                Case "Scatter", "Line"
                                        .MarkerForegroundColor = l_Cell.DisplayFormat.Borders(xlEdgeTop).Color
                                        
                                Case "Bar", "Column", "Pie"
                                    Err.Clear
                                    .Format.Line.Visible = msoFalse 'for Line.ForeColor getting to work we have to cheat something
                                    .Format.Line.Visible = msoTrue
                                    .Format.Line.ForeColor.RGB = l_Cell.DisplayFormat.Borders(xlEdgeTop).Color
                                End Select
                        Else
                            
                            Select Case l_ChartType
                                Case "Scatter", "Line"
                                    .MarkerForegroundColor = l_Cell.DisplayFormat.Interior.Color
                                Case "Bar", "Column"
                                    .Format.Line.Visible = msoFalse
                                    '.Format.Line.Visible = msoTrue
                                    '.Format.Line.ForeColor = l_Cell.DisplayFormat.Interior.Color
                                End Select
                        End If
                        
                    End With
                    
                    Err.Clear
                    On Error GoTo 0
                    
                    
                Next
                
            End With
            
        Next
    
    End With

End Sub

Private Sub c_Chart_Calculate()
    FormatChart
End Sub

Private Function ConvertLineToDash(ByVal pLineStyle As XlLineStyle) As Double
    'The values of the border line style are not the same of the line style on the charts
    'this function will convert

    Select Case pLineStyle
        Case xlContinuous
            ConvertLineToDash = msoLineSolid
        Case xlDash
            ConvertLineToDash = msoLineDash
        Case xlDashDot
            ConvertLineToDash = msoLineDashDot
        Case xlDashDotDot
            ConvertLineToDash = msoLineDashDotDot
        Case xlDot
            ConvertLineToDash = msoLineRoundDot
        Case xlDouble
            ConvertLineToDash = msoLineSolid
        Case xlLineStyleNone
            ConvertLineToDash = msoLineSysDot
        Case xlSlantDashDot
            ConvertLineToDash = msoLineSysDashDot
        Case Else
            ConvertLineToDash = msoLineSolid
    End Select

End Function

Private Function ColorToRGB(ByVal c As Double) As Variant

r = c Mod 256
g = c \ 256 Mod 256
b = c \ 65536 Mod 256

ColorToRGB = RGB(r, g, b)

End Function
