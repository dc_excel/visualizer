VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ChartFormatterApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public WithEvents App As Application
Attribute App.VB_VarHelpID = -1


Private Sub App_SheetActivate(ByVal Sh As Object)
    InvalidateRibbon
End Sub

Private Sub App_SheetChange(ByVal Sh As Object, ByVal Target As Range)
    Dim c As ConditionalChart
    
    On Error Resume Next
    For Each c In g_ChartFormatter.Charts
        If Err.Number <> 0 Then
            Err.Clear
            g_ChartFormatter.Charts.Remove c
        End If
        
        If Not c.LabelRange Is Nothing Then
            If Target.Parent.Name = c.LabelRange.Parent.Name Then
                If Not Intersect(Target, c.LabelRange) Is Nothing Then
                    c.FormatChart
                End If
            End If
        End If
    Next
    On Error GoTo 0

End Sub

Private Sub App_SheetSelectionChange(ByVal Sh As Object, ByVal Target As Range)
InvalidateRibbon
End Sub
Private Sub App_WorkbookActivate(ByVal Wb As Workbook)
InvalidateRibbon
End Sub

Private Sub App_WorkbookBeforeSave(ByVal Wb As Workbook, ByVal SaveAsUI As Boolean, Cancel As Boolean)
    If Not g_ChartFormatter Is Nothing Then
        g_ChartFormatter.SaveSettings
    End If
    
End Sub

Private Sub App_WorkbookOpen(ByVal Wb As Workbook)
InvalidateRibbon

End Sub

Private Sub Class_Terminate()
    Set App = Nothing
End Sub

