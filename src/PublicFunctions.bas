Attribute VB_Name = "PublicFunctions"
Public Function SUBFIELD(ByVal pText As Variant, _
                            ByVal pDelimiter As String, _
                            Optional ByVal pOccurance As Double = 1) As Variant
                            
        'split a string based on a delimiter, and return part of the string
        'Can be used as a normal string function or as an array function in the worksheet
        
        Dim l_arr_String() As String
        Dim l_lng_CallRows As Long
        Dim l_lng_CallCols As Long
        
        'Check with called as an array function from multiple cells
        Err.Clear
        On Error Resume Next
        If Not IsError(Application.Caller.Rows.Count) Then
            If Err.Number = 0 Then
            
                With Application.Caller
                    l_lng_CallRows = .Rows.Count
                    l_lng_CallCols = .Columns.Count
                End With
            Else
                Err.Clear
                l_lng_CallRows = 1
                l_lng_CallCols = 1
            End If
        Else
            l_lng_CallRows = 1
            l_lng_CallCols = 1
        End If
        On Error GoTo 0
        
        'Create array of results based on delimiter
        l_arr_String = Split(pText, pDelimiter)
        
        'If called by a single cell
        If l_lng_CallRows = 1 And l_lng_CallCols = 1 Then
            
            If pOccurance > 0 Then
                SUBFIELD = l_arr_String(pOccurance - 1)
            Else 'If pOccurance is negative, start from the end of the string
                SUBFIELD = l_arr_String(UBound(l_arr_String) + pOccurance + 1)
            End If
            
        Else
             'Create an array of results
             Dim result() As String
             Dim RowNdx As Long
             Dim ColNdx As Long
             ReDim result(1 To l_lng_CallRows, 1 To l_lng_CallCols)
             
             'Loop through calling cells and populate with results
             For RowNdx = 1 To l_lng_CallRows
               For ColNdx = 1 To l_lng_CallCols
                    If RowNdx + ColNdx - 2 <= UBound(l_arr_String) Then
                        result(RowNdx, ColNdx) = l_arr_String(RowNdx + ColNdx - 2)
                    End If
               Next ColNdx
             Next RowNdx
            
            'return array
            SUBFIELD = result
        End If
                            
                            
 End Function
