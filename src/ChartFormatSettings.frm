VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ChartFormatSettings 
   Caption         =   "ChartName"
   ClientHeight    =   3765
   ClientLeft      =   105
   ClientTop       =   450
   ClientWidth     =   10065
   OleObjectBlob   =   "ChartFormatSettings.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "ChartFormatSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub bSave_Click()
    
        'Set the label range property and redraw the chart
        With g_ChartFormatter.CurrentConditionalChart
            Set .LabelRange = Range(Me.rLabelsRange)
            .FormatChart
        End With
        
        Unload Me
End Sub

Private Sub UserForm_Initialize()

    With g_ChartFormatter.CurrentConditionalChart
    
        Me.Caption = .Chart.Parent.Name & " - " & .Chart.Name
        
        If Not .LabelRange Is Nothing Then
            Me.rLabelsRange = .LabelRange.Parent.Name & "!" & .LabelRange.Address
        End If
    
    End With

End Sub

