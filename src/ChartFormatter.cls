VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ChartFormatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private WithEvents c_Workbook As Workbook
Attribute c_Workbook.VB_VarHelpID = -1
Private c_Charts As Collection
Private c_App As Application
Private c_CurrentConditionalChart As ConditionalChart


Private Sub Class_Initialize()

    Set c_Charts = New Collection
    Set c_Workbook = ActiveWorkbook
    
    LoadSettings

End Sub

Public Property Get CurrentConditionalChart() As ConditionalChart
    Set CurrentConditionalChart = c_CurrentConditionalChart
End Property
Public Property Set CurrentConditionalChart(pCC As ConditionalChart)
    Set c_CurrentConditionalChart = pCC
End Property

Sub AddChart()
    'Adds the active chart to the array of conditionally formatted charts
    
    Dim cc As ConditionalChart
    
    If ActiveChart Is Nothing Then
        Exit Sub
    End If
    
    'in case some version of the current chart is already in the array
    RemoveChart
     
     Set cc = New ConditionalChart
     Set cc.Chart = ActiveChart
        
     c_Charts.Add cc
     c_Charts(c_Charts.Count).FormatChart
     
End Sub

Function ChartExists() As Boolean
    'Check if the chart still exists in the workbook
    'If it doesn't we will remove it from the array
    
    Dim c As ConditionalChart
    Dim s As String
    On Error Resume Next
    
    If Not ActiveChart Is Nothing Then
        For k = c_Charts.Count To 1 Step -1
            
            Set c = c_Charts(k)
            s = c.Chart.Name
            
        
            If c.Chart.Name = ActiveChart.Name And c.Chart.Parent.Name = ActiveChart.Parent.Name Then
                If Err.Number <> 0 Then
                    Err.Clear
                    c_Charts.Remove k
                Else
                    Set c_CurrentConditionalChart = c
                    ChartExists = True
                
                    Exit Function
                End If
                
            End If
            
        
        Next k
    End If
    
    Set c_CurrentConditionalChart = Nothing
    ChartExists = False
    On Error GoTo 0

End Function

Sub RemoveChart()
    'Remove the active chart from the array
    
    If ActiveChart Is Nothing Then
        Exit Sub
    End If
    
    On Error Resume Next
    For k = c_Charts.Count To 1 Step -1
        If k > c_Charts.Count Then
            Exit For
        End If
        
        If c_Charts(k).Chart.Name = ActiveChart.Name And c_Charts(k).Chart.Parent.Name = ActiveChart.Parent.Name Then
            If Err.Number > 0 Then
                c_Charts.Remove k
            Else
                c_Charts.Remove k
            End If
        End If
    Next
    Err.Clear
    On Error GoTo 0

End Sub

Sub ValidateCharts()
'Check that all charts still exist within the workbook
'Remove those that don't

Dim sTest As String

    On Error Resume Next
    For k = c_Charts.Count To 1 Step -1
        If k > c_Charts.Count Then
            Exit For
        End If
        
        sTest = c_Charts(k).Chart.Name
        sTest = c_Charts(k).Chart.Parent.Name
        
            If Not c_Charts(k).LabelRange Is Nothing Then
                sTest = c_Charts(k).LabelRange.Parent.Name
            End If
        If Err.Number > 0 Then
            Err.Clear
            c_Charts.Remove k
        End If
       
    Next
    
    
    On Error GoTo 0
End Sub

Public Property Get Charts() As Collection
    Set Charts = c_Charts
End Property



Private Sub CollectExistingCharts()
    
    Dim sht As Worksheet
    Dim cht As ChartObject
    Dim cc As ConditionalChart
    Dim CurrentSheet As Worksheet
    
    On Error GoTo errhand
    
    Application.ScreenUpdating = False
    Application.EnableEvents = False
    
    Set CurrentSheet = ActiveSheet
    
    For Each sht In c_Workbook.Worksheets
      For Each cht In sht.ChartObjects
        cht.Activate
        
        Set cc = New ConditionalChart
        Set cc.Chart = cht.Chart
        
        c_Charts.Add cc
      Next cht
    Next sht
    
errhand:
    If Err.Number > 0 Then
        Debug.Print Err.Description
        Err.Clear
    End If
    CurrentSheet.Activate
    Application.EnableEvents = True
    Application.ScreenUpdating = True
    On Error GoTo 0

End Sub


Sub FormatCharts()
    
    Dim l_chart As Chart
    Dim CurrentSheet As Worksheet
    
    On Error GoTo errhand
    
    Application.ScreenUpdating = False
    Application.EnableEvents = False
    
    Set CurrentSheet = ActiveSheet

    For k = 1 To c_Charts.Count
    
        'BEcause we are removing chart in this loop, and subtracting 1 from the looper
        'We need to check that we haven't gone over the number of possible charts after a deletion
        If k > c_Charts.Count Then
            Exit For
        End If
        
            'we will want to catch error for charts that have been deleted and remove them from the collection
            On Error Resume Next
        
            Set l_chart = c_Charts(k).Chart
            l_chart.Parent.Activate
            
            'Chart Object no longer exists, remove it from the collection and continue
            If Err.Number = 424 Then
                c_Charts.Remove k
                k = k - 1
                Err.Clear
            ElseIf Err.Number > 0 Then
                GoTo errhand
            End If

        'DO SOMETHING WITH CHARTS
        c_Charts(k).FormatChart
    
    Next k

errhand:
    If Err.Number > 0 Then
        Debug.Print Err.Description
        Err.Clear
    End If
    
    CurrentSheet.Activate
    Application.EnableEvents = True
    Application.ScreenUpdating = True
    On Error GoTo 0

End Sub

Sub SaveSettings()
 'Save the conditional chart settings to the workbook document properties
 Dim c As ConditionalChart
 Dim lRange As String
 
 ClearExistingSettings
 ValidateCharts
 
 For k = 1 To c_Charts.Count
 
   Set c = c_Charts(k)
    
    If Not c.LabelRange Is Nothing Then
        lRange = c.LabelRange.Parent.Name & "!" & c.LabelRange.Address
    Else
        lRange = ""
    End If
   
    With c_Workbook.CustomDocumentProperties
        .Add Name:="ConditionalChart" & k, _
                LinkToContent:=False, _
                Type:=msoPropertyTypeString, _
                Value:=c.Chart.Name & "|||" & c.Chart.Parent.Parent.Name & "|||" & lRange
                
    End With
    
 Next
 
End Sub

Sub ClearExistingSettings()
    'Remove conditional charts from custom document properties
    For Each p In c_Workbook.CustomDocumentProperties
        If p.Name Like "ConditionalChart*" Then
            p.Delete
        End If
    Next

End Sub

Sub LoadSettings()
    'Restore conditional charts form the custom document properties
    Dim pChartName As String
    Dim pChartSheet As String
    Dim pChartLabelRange As String
    Dim pCC As ConditionalChart

    Application.ScreenUpdating = False
    Application.EnableEvents = False
    On Error Resume Next
    For Each p In c_Workbook.CustomDocumentProperties
        If p.Name Like "ConditionalChart*" Then
            
            pChartName = SUBFIELD(p.Value, "|||", 1)
            pChartSheet = SUBFIELD(p.Value, "|||", 2)
            pChartLabelRange = SUBFIELD(p.Value, "|||", 3)
            
            For Each cht In c_Workbook.Sheets(pChartSheet).ChartObjects
                If cht.Chart.Name = pChartName Then
                    Set pCC = New ConditionalChart
                    
                    With pCC
                        Set .Chart = cht.Chart
                    
                    If pChartLabelRange <> "" Then
                        Set .LabelRange = Range(pChartLabelRange)
                    End If
                    
                        .FormatChart
                    End With
                    
                    c_Charts.Add pCC
                    
                End If
            Next
            
        End If
    Next
    
    Application.ScreenUpdating = True
    Application.EnableEvents = True
    On Error GoTo 0
    
End Sub

Private Function WorksheetExists(shtName As String, Optional Wb As Workbook) As Boolean
    'Validate a worksheet exists in a workbook by name
    Dim sht As Worksheet

    If Wb Is Nothing Then Set Wb = ThisWorkbook
    On Error Resume Next
    Set sht = Wb.Sheets(shtName)
    On Error GoTo 0
    WorksheetExists = Not sht Is Nothing
End Function
